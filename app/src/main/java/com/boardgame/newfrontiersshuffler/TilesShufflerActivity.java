package com.boardgame.newfrontiersshuffler;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.boardgame.newfrontiersshuffler.model.Tile;
import com.boardgame.newfrontiersshuffler.model.TileCollection;

import java.util.List;

public class TilesShufflerActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private MyAdapter myAdapter;
    private List<Tile> listOfTilesToSendToAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tiles_shuffler);

        // setting the toolbar
        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        // setting the back arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //creating a tile collection
        TileCollection tileCollection = TileCollection.getInstance();

        //receiving the intent from main activity
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        // big/small tiles list is sent to adapter depending on the button clicked in the main activity
        if (extras.getInt("size") == 0) {
            listOfTilesToSendToAdapter = tileCollection.getSmallTiles();
            setTitle(getResources().getString(R.string.small_tiles_shuffler));
        } else {
            listOfTilesToSendToAdapter = tileCollection.getBigTiles();
            setTitle(getResources().getString(R.string.big_tiles_shuffler));
        }

        //connecting the adapter and the viewpager
        viewPager = findViewById(R.id.vpPager);
        myAdapter = new MyAdapter(this, getSupportFragmentManager(), listOfTilesToSendToAdapter);
        viewPager.setAdapter(myAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}
