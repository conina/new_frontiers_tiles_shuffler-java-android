package com.boardgame.newfrontiersshuffler;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.boardgame.newfrontiersshuffler.model.Tile;

import java.util.List;

public class MyAdapter extends FragmentPagerAdapter {

    static final private int NUM_PAGES = 8;
    private List <Tile> mListOfTiles;
    private Context mContext;


    public MyAdapter(Context context, FragmentManager fm, List<Tile> list) {
        super(fm);
        mListOfTiles = list;
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position >= mListOfTiles.size()) {
            return TileFragment.newInstance(new Tile (mContext.getResources().getString(R.string.empty_tile), mContext.getResources().getString(R.string.empty_tile)));
        } else {
            return TileFragment.newInstance(mListOfTiles.get(position));
        }
            }

    @Override
    public CharSequence getPageTitle(int position) {
        return  mContext.getResources().getString(R.string.tile) + (position + 1);

    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}
