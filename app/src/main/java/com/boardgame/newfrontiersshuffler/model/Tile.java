package com.boardgame.newfrontiersshuffler.model;

import android.util.Log;

import java.util.Random;

public class Tile {

    private String frontSide;
    private String backSide;

    private static final int TILE_FRONT=0;
    private static final int TILE_BACK=1;

    private int current_orientation;

    public Tile(String backSide, String frontSide) {
        this.backSide = backSide;
        this.frontSide = frontSide;
        this.randomFlip();
    }

    public void randomFlip() {
        this.current_orientation = new Random().nextInt(2);
    }

    private String getFrontSide() {
        return frontSide;
    }

    private String getBackSide() {
        return backSide;
    }

    public String getCurrentSide() {
        switch (this.current_orientation) {
            case TILE_FRONT:
                return frontSide;
            case TILE_BACK:
                return backSide;
            default:
                Log.e("TileShuffler",
                      "Oops, something went wrong. Bad orientation id: " + current_orientation);
        }

        Random random = new Random();
        int randomNum = random.nextInt(2);

        Log.d("Side of the tile", Integer.toString(randomNum));

        if (randomNum == 0) {
            return getFrontSide();
        } else {
            return getBackSide();
        }
    }
}
