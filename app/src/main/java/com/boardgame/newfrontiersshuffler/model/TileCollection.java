package com.boardgame.newfrontiersshuffler.model;

import java.util.ArrayList;
import java.util.List;

public final class TileCollection {

    private List<Tile> smallTiles;
    private List<Tile> bigTiles;
    private static TileCollection instance = null;

    public List<Tile> getSmallTiles() {
        return smallTiles;
    }

    public List<Tile> getBigTiles() {
        return bigTiles;
    }

    private TileCollection() {
        smallTiles = new ArrayList<>();
        bigTiles = new ArrayList<>();

        populateLists();
    }

    public static TileCollection getInstance() {
        if (instance == null) {
            instance = new TileCollection();
        }
        return instance;
    }

  public void shuffleTiles () {
        for (Tile tile: bigTiles) {
            tile.randomFlip();
        }
        for (Tile tile: smallTiles) {
            tile.randomFlip();
        }
  }

    private void populateLists() {

        //populate small Tiles
        smallTiles.add(new Tile("s_tile1_a", "s_tile1_b"));
        smallTiles.add(new Tile("s_tile2_a", "s_tile2_b"));
        smallTiles.add(new Tile("s_tile3_a", "s_tile3_b"));
        smallTiles.add(new Tile("s_tile4_a", "s_tile4_b"));
        smallTiles.add(new Tile("s_tile5_a", "s_tile5_b"));
        smallTiles.add(new Tile("s_tile6_a", "s_tile6_b"));
        smallTiles.add(new Tile("s_tile7_a", "s_tile7_b"));
        smallTiles.add(new Tile("s_tile8_a", "s_tile8_b"));

        //populate big Tiles
        bigTiles.add(new Tile("tile1_a", "tile1_b"));
        bigTiles.add(new Tile("tile2_a", "tile2_b"));
        bigTiles.add(new Tile("tile3_a", "tile3_b"));
        bigTiles.add(new Tile("tile4_a", "tile4_b"));
        bigTiles.add(new Tile("tile5_a", "tile5_b"));
        bigTiles.add(new Tile("tile6_a", "tile6_b"));
        bigTiles.add(new Tile("tile7_a", "tile7_b"));
        bigTiles.add(new Tile("tile8_a", "tile8_b"));
    }
}
