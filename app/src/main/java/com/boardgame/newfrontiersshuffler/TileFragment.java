package com.boardgame.newfrontiersshuffler;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.boardgame.newfrontiersshuffler.model.Tile;

public class TileFragment extends Fragment {

    private String tileSide;

    static TileFragment newInstance(Tile tile) {

        TileFragment tileFragment = new TileFragment();
        Bundle args = new Bundle();

        //putting the information about which side of the tile should be shown
        args.putString("side", tile.getCurrentSide());
        tileFragment.setArguments(args);
        return tileFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //unpacking the bundle where we put the information about the side of the tile
        tileSide = getArguments() != null ? getArguments().getString("side") : "nothing_found";
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.tile_fragment_layout, container, false);
        ImageView tileImage = view.findViewById(R.id.tile_image);

        //pulling the image from the resources and setting it to the imageview for that image
        int imageIdentifier = getResources().getIdentifier(tileSide, "drawable", "com.example.newfrontiersshuffler");
        tileImage.setImageResource(imageIdentifier);

        return view;
    }
}


