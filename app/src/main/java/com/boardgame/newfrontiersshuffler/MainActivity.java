package com.boardgame.newfrontiersshuffler;

import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;

import androidx.fragment.app.FragmentActivity;

import com.boardgame.newfrontiersshuffler.model.TileCollection;

public class MainActivity extends FragmentActivity {

    private final int MILLISECONDS = 80;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    //button for the small tiles shuffler window
    public void smallTilesShuffler(View view) {
        Intent intent = new Intent(this, TilesShufflerActivity.class);
        intent.putExtra(getResources().getString(R.string.size), 0);
        startActivity(intent);

    }

    //button for the big tiles shuffler window
    public void bigTilesShuffler(View view) {
        Intent intent = new Intent(this, TilesShufflerActivity.class);
        intent.putExtra(getResources().getString(R.string.size), 1);
        startActivity(intent);
    }

    //button for shuffling cards
    public void shuffleAll(View view) {
        final Vibrator vibe = (Vibrator) getSystemService(this.VIBRATOR_SERVICE);

        vibe.vibrate(MILLISECONDS);
        TileCollection tileCollection = TileCollection.getInstance();
        tileCollection.shuffleTiles();
    }
}
